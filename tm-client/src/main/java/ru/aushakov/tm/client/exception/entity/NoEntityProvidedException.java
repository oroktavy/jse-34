package ru.aushakov.tm.client.exception.entity;

public class NoEntityProvidedException extends RuntimeException {

    public NoEntityProvidedException() {
        super("Null record provided for operation!");
    }

}
