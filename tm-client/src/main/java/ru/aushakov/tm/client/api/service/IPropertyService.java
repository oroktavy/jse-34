package ru.aushakov.tm.client.api.service;

import ru.aushakov.tm.client.enumerated.ConfigProperty;

public interface IPropertyService {

    String getProperty(ConfigProperty property);

    Integer getIntProperty(ConfigProperty property);

}
