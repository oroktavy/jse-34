package ru.aushakov.tm.client.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractUserCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.exception.entity.UserNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Optional;

public final class UserRemoveByLoginCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.USER_REMOVE_BY_LOGIN;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove user by login";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER USER LOGIN:");
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        @Nullable final String login = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getDataService().getAdminEndpointPort().removeUserByLogin(login, session))
                .orElseThrow(UserNotFoundException::new);
    }

}
