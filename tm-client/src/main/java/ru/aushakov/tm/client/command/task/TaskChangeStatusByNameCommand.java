package ru.aushakov.tm.client.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractTaskCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.enumerated.Status;
import ru.aushakov.tm.client.exception.entity.TaskNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.TASK_CHANGE_STATUS_BY_NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change task status by name";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS " + Arrays.toString(Status.values()) + ":");
        @Nullable final String statusId = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getTaskService()
                .getTaskEndpointPort().changeTaskStatusByName(name, statusId, session))
                .orElseThrow(TaskNotFoundException::new);
    }

}
