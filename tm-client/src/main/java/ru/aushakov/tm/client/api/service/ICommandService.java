package ru.aushakov.tm.client.api.service;

import ru.aushakov.tm.client.api.ServiceLocator;
import ru.aushakov.tm.client.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    Collection<AbstractCommand> getTerminalCommands();

    AbstractCommand getCommandByName(String command);

    AbstractCommand getCommandByArg(String arg);

    void add(AbstractCommand command);

    void initCommands(ServiceLocator serviceLocator);

}
