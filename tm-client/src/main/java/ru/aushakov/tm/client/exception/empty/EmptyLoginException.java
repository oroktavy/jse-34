package ru.aushakov.tm.client.exception.empty;

public class EmptyLoginException extends RuntimeException {

    public EmptyLoginException() {
        super("Provided login is empty!");
    }

}
