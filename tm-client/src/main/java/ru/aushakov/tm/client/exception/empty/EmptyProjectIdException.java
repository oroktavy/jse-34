package ru.aushakov.tm.client.exception.empty;

public class EmptyProjectIdException extends RuntimeException {

    public EmptyProjectIdException() {
        super("Provided project id is empty!");
    }

}
