package ru.aushakov.tm.client.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.client.command.AbstractProjectCommand;
import ru.aushakov.tm.client.constant.TerminalConst;
import ru.aushakov.tm.client.endpoint.Session;
import ru.aushakov.tm.client.enumerated.Role;
import ru.aushakov.tm.client.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.client.util.TerminalUtil;

import java.util.Optional;

public final class ProjectDeepDeleteByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return TerminalConst.PROJECT_DEEP_DELETE_BY_ID;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete project with child tasks";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getAuthService().getSession();
        System.out.println("[REMOVE PROJECT WITH CHILD TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        Optional.ofNullable(serviceLocator.getProjectService()
                .getProjectEndpointPort().deepDeleteProjectById(id, session))
                .orElseThrow(ProjectNotFoundException::new);
    }

}
