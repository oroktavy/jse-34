package ru.aushakov.tm.client.api;

import ru.aushakov.tm.client.api.service.IAuthService;
import ru.aushakov.tm.client.api.service.ICommandService;
import ru.aushakov.tm.client.api.service.IPropertyService;
import ru.aushakov.tm.client.endpoint.*;

public interface ServiceLocator {

    ICommandService getCommandService();

    IAuthService getAuthService();

    IPropertyService getPropertyService();

    ProjectEndpointService getProjectService();

    TaskEndpointService getTaskService();

    UserEndpointService getUserService();

    AdminEndpointService getDataService();

    SessionEndpointService getSessionService();

}
