package ru.aushakov.tm.client.exception.general;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class InvalidIndexException extends RuntimeException {

    public InvalidIndexException() {
        super("Index is invalid!");
    }

    public InvalidIndexException(@NotNull final Integer index) {
        super("The value '" + index + "' entered is not a valid index!");
    }

    public InvalidIndexException(@Nullable final String textIndex) {
        super("The value '" + textIndex + "' entered is not a number!");
    }

}
