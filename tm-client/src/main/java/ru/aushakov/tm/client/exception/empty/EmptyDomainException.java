package ru.aushakov.tm.client.exception.empty;

public class EmptyDomainException extends RuntimeException {

    public EmptyDomainException() {
        super("No domain provided!");
    }

}
