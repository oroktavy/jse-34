
package ru.aushakov.tm.client.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.aushakov.tm.server.endpoint package.
 * &lt;p&gt;An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AddUser_QNAME = new QName("http://endpoint.server.tm.aushakov.ru/", "addUser");
    private final static QName _AddUserResponse_QNAME = new QName("http://endpoint.server.tm.aushakov.ru/", "addUserResponse");
    private final static QName _ChangePassword_QNAME = new QName("http://endpoint.server.tm.aushakov.ru/", "changePassword");
    private final static QName _ChangePasswordResponse_QNAME = new QName("http://endpoint.server.tm.aushakov.ru/", "changePasswordResponse");
    private final static QName _ViewProfile_QNAME = new QName("http://endpoint.server.tm.aushakov.ru/", "viewProfile");
    private final static QName _ViewProfileResponse_QNAME = new QName("http://endpoint.server.tm.aushakov.ru/", "viewProfileResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.aushakov.tm.server.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddUser }
     */
    public AddUser createAddUser() {
        return new AddUser();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link ChangePassword }
     */
    public ChangePassword createChangePassword() {
        return new ChangePassword();
    }

    /**
     * Create an instance of {@link ChangePasswordResponse }
     */
    public ChangePasswordResponse createChangePasswordResponse() {
        return new ChangePasswordResponse();
    }

    /**
     * Create an instance of {@link ViewProfile }
     */
    public ViewProfile createViewProfile() {
        return new ViewProfile();
    }

    /**
     * Create an instance of {@link ViewProfileResponse }
     */
    public ViewProfileResponse createViewProfileResponse() {
        return new ViewProfileResponse();
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link AdaptedUser }
     */
    public AdaptedUser createAdaptedUser() {
        return new AdaptedUser();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.tm.aushakov.ru/", name = "addUser")
    public JAXBElement<AddUser> createAddUser(AddUser value) {
        return new JAXBElement<AddUser>(_AddUser_QNAME, AddUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.tm.aushakov.ru/", name = "addUserResponse")
    public JAXBElement<AddUserResponse> createAddUserResponse(AddUserResponse value) {
        return new JAXBElement<AddUserResponse>(_AddUserResponse_QNAME, AddUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePassword }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ChangePassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.tm.aushakov.ru/", name = "changePassword")
    public JAXBElement<ChangePassword> createChangePassword(ChangePassword value) {
        return new JAXBElement<ChangePassword>(_ChangePassword_QNAME, ChangePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePasswordResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ChangePasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.tm.aushakov.ru/", name = "changePasswordResponse")
    public JAXBElement<ChangePasswordResponse> createChangePasswordResponse(ChangePasswordResponse value) {
        return new JAXBElement<ChangePasswordResponse>(_ChangePasswordResponse_QNAME, ChangePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewProfile }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ViewProfile }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.tm.aushakov.ru/", name = "viewProfile")
    public JAXBElement<ViewProfile> createViewProfile(ViewProfile value) {
        return new JAXBElement<ViewProfile>(_ViewProfile_QNAME, ViewProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewProfileResponse }{@code >}
     *
     * @param value Java instance representing xml element's value.
     * @return the new instance of {@link JAXBElement }{@code <}{@link ViewProfileResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.server.tm.aushakov.ru/", name = "viewProfileResponse")
    public JAXBElement<ViewProfileResponse> createViewProfileResponse(ViewProfileResponse value) {
        return new JAXBElement<ViewProfileResponse>(_ViewProfileResponse_QNAME, ViewProfileResponse.class, null, value);
    }

}
