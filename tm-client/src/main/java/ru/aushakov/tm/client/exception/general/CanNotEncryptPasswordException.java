package ru.aushakov.tm.client.exception.general;

import org.jetbrains.annotations.NotNull;

public class CanNotEncryptPasswordException extends RuntimeException {

    public CanNotEncryptPasswordException() {
        super("Can not encrypt password using MD5! Input value may be empty!");
    }

    public CanNotEncryptPasswordException(@NotNull Throwable cause) {
        super("Can not encrypt password using MD5!", cause);
    }

    public CanNotEncryptPasswordException(@NotNull Integer iterationNum) {
        super("Can not encrypt password using MD5! Wrong iteration property passed!");
    }

}
