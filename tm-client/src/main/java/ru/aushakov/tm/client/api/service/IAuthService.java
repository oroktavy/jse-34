package ru.aushakov.tm.client.api.service;

import ru.aushakov.tm.client.endpoint.Session;

public interface IAuthService {

    boolean isUserAuthenticated();

    Session getSession();

    void setSession(Session session);

    String getUserId();

}
