package ru.aushakov.tm.client.exception.empty;

public class EmptyArgumentException extends RuntimeException {

    public EmptyArgumentException() {
        super("Provided argument is empty!");
    }

}
