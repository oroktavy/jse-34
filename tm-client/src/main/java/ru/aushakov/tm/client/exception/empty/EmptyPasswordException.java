package ru.aushakov.tm.client.exception.empty;

public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Password can not be empty!");
    }

}
