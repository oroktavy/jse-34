package ru.aushakov.tm.client.exception.empty;

public class EmptySessionException extends RuntimeException {

    public EmptySessionException() {
        super("No session provided!");
    }

}
