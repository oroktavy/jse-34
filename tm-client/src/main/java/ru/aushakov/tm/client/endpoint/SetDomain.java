
package ru.aushakov.tm.client.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for setDomain complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="setDomain"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="domain" type="{http://endpoint.server.tm.aushakov.ru/}domain" minOccurs="0"/&amp;gt;
 * &amp;lt;element name="session" type="{http://endpoint.server.tm.aushakov.ru/}session" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setDomain", propOrder = {
        "domain",
        "session"
})
public class SetDomain {

    protected Domain domain;
    protected Session session;

    /**
     * Gets the value of the domain property.
     *
     * @return possible object is
     * {@link Domain }
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * Sets the value of the domain property.
     *
     * @param value allowed object is
     *              {@link Domain }
     */
    public void setDomain(Domain value) {
        this.domain = value;
    }

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link Session }
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link Session }
     */
    public void setSession(Session value) {
        this.session = value;
    }

}
