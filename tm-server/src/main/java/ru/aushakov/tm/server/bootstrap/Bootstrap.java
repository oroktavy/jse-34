package ru.aushakov.tm.server.bootstrap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import ru.aushakov.tm.server.api.ServiceLocator;
import ru.aushakov.tm.server.api.repository.IProjectRepository;
import ru.aushakov.tm.server.api.repository.ISessionRepository;
import ru.aushakov.tm.server.api.repository.ITaskRepository;
import ru.aushakov.tm.server.api.repository.IUserRepository;
import ru.aushakov.tm.server.api.service.*;
import ru.aushakov.tm.server.component.Backuper;
import ru.aushakov.tm.server.endpoint.AbstractEndpoint;
import ru.aushakov.tm.server.enumerated.Status;
import ru.aushakov.tm.server.model.Project;
import ru.aushakov.tm.server.model.Task;
import ru.aushakov.tm.server.repository.ProjectRepository;
import ru.aushakov.tm.server.repository.SessionRepository;
import ru.aushakov.tm.server.repository.TaskRepository;
import ru.aushakov.tm.server.repository.UserRepository;
import ru.aushakov.tm.server.service.*;
import ru.aushakov.tm.server.util.SystemUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, Task.class);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository, Project.class);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userRepository, propertyService);

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(
            sessionRepository,
            userRepository,
            propertyService
    );

    @NotNull
    private final IDataService dataService = new DataService(propertyService, projectService, taskService, userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Backuper backuper = new Backuper(this);

    public void initData() {
        userService.add("TEST", "TEST", "test@tsconsulting.com");
        userService.add("ADMIN", "ADMIN", "admin@tsconsulting.com", "admin");

        @NotNull final String testId = userService.findOneByLogin("TEST").getId();
        @NotNull final String adminId = userService.findOneByLogin("ADMIN").getId();
        taskService.add("DEMO 2", "222222222222", testId).setStatus(Status.IN_PROGRESS);
        taskService.add("DEMO 3", "333333333333", testId);
        taskService.add("DEMO 1", "111111111111", adminId).setStatus(Status.COMPLETED);
        projectService.add("DEMO 2", "222222222222", adminId).setStatus(Status.IN_PROGRESS);
        projectService.add("DEMO 3", "333333333333", testId);
        projectService.add("DEMO 1", "111111111111", adminId).setStatus(Status.COMPLETED);
    }

    @SneakyThrows
    private void initEndpoints() {
        @NotNull final Reflections reflections = new Reflections("ru.aushakov.tm.server.endpoint");
        @NotNull final Set<Class<? extends AbstractEndpoint>> classes =
                reflections.getSubTypesOf(AbstractEndpoint.class);
        @NotNull List<Class<? extends AbstractEndpoint>> sortedClasses =
                classes.stream()
                        .sorted((Comparator.comparing(Class::getName)))
                        .collect(Collectors.toList());
        for (@NotNull final Class<? extends AbstractEndpoint> clazz : sortedClasses) {
            int mods = clazz.getModifiers();
            if (Modifier.isAbstract(mods) || Modifier.isInterface(mods)) continue;
            @NotNull AbstractEndpoint endpoint = clazz.newInstance();
            endpoint.init(this);
        }
    }

    @SneakyThrows
    private void createPIDFile() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void run() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        createPIDFile();
        backuper.init();
        initEndpoints();
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IDataService getDataService() {
        return dataService;
    }

    @Override
    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

}
