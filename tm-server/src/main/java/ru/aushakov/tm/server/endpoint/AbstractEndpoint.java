package ru.aushakov.tm.server.endpoint;

import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.server.api.ServiceLocator;
import ru.aushakov.tm.server.api.service.*;
import ru.aushakov.tm.server.enumerated.ConfigProperty;

import javax.xml.ws.Endpoint;

public abstract class AbstractEndpoint {

    @NonNull
    protected ServiceLocator serviceLocator;

    @NonNull
    protected IProjectService projectService;

    @NonNull
    protected ITaskService taskService;

    @NonNull
    protected IUserService userService;

    @NonNull
    protected ISessionService sessionService;

    @NonNull
    protected IDataService dataService;

    public void init(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
        this.projectService = serviceLocator.getProjectService();
        this.taskService = serviceLocator.getTaskService();
        this.userService = serviceLocator.getUserService();
        this.sessionService = serviceLocator.getSessionService();
        this.dataService = serviceLocator.getDataService();
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull String wsAddress = propertyService.getProperty(ConfigProperty.WS_PROTOCOL) + "://"
                + propertyService.getProperty(ConfigProperty.WS_HOST);
        @NotNull final String port = propertyService.getProperty(ConfigProperty.WS_PORT);
        if (!StringUtils.isEmpty(port)) wsAddress += ":" + port;
        wsAddress += "/" + this.getClass().getSimpleName() + "?wsdl";
        Endpoint.publish(wsAddress, this);
        System.out.println(wsAddress);
    }

}
