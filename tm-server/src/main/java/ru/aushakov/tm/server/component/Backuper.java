package ru.aushakov.tm.server.component;

import com.fasterxml.jackson.databind.AnnotationIntrospector;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationIntrospector;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.aushakov.tm.server.api.service.IDataService;
import ru.aushakov.tm.server.bootstrap.Bootstrap;
import ru.aushakov.tm.server.dto.Domain;
import ru.aushakov.tm.server.enumerated.ConfigProperty;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backuper implements Runnable {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backuper(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    private void load() {
        @NotNull final IDataService dataService = bootstrap.getDataService();
        @NotNull final String fileXml = dataService.getFileBackup();
        @NotNull final File backup = new File(fileXml);
        if (backup.exists()) {
            @NotNull final String xml = new String(Files.readAllBytes(Paths.get(fileXml)));
            @NotNull final ObjectMapper objectMapper = new XmlMapper();
            @NotNull final AnnotationIntrospector annotationIntrospector =
                    new JaxbAnnotationIntrospector(objectMapper.getTypeFactory());
            objectMapper.setAnnotationIntrospector(annotationIntrospector);
            @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
            dataService.setDomain(domain);
        } else {
            bootstrap.initData();
        }
    }

    @SneakyThrows
    public void run() {
        @NotNull final IDataService dataService = bootstrap.getDataService();
        @NotNull final Domain domain = dataService.getDomain();
        @NotNull final String fileXml = dataService.getFileBackup();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final AnnotationIntrospector annotationIntrospector =
                new JaxbAnnotationIntrospector(objectMapper.getTypeFactory());
        objectMapper.setAnnotationIntrospector(annotationIntrospector);
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileXml);
        fileOutputStream.write(xml.getBytes(StandardCharsets.UTF_8));
        fileOutputStream.close();
    }

    public void init() {
        load();
        start();
    }

    private void start() {
        @NotNull final Integer sleepTime =
                bootstrap.getPropertyService().getIntProperty(ConfigProperty.BACKUP_SLEEP_TIME);
        executorService.scheduleWithFixedDelay(this, 0, sleepTime, TimeUnit.SECONDS);
    }

    private void stop() {
        executorService.shutdown();
    }

}
