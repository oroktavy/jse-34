package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.dto.Domain;

public interface IDataService {

    String getFileBinary();

    String getFileBase64();

    String getFileJson();

    String getFileXml();

    String getFileXmlForJaxB();

    String getFileBackup();

    Domain getDomain();

    void setDomain(Domain domain);

}
