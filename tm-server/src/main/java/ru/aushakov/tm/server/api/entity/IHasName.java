package ru.aushakov.tm.server.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
