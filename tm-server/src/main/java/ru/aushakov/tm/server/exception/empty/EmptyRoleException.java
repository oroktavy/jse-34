package ru.aushakov.tm.server.exception.empty;

public class EmptyRoleException extends RuntimeException {

    public EmptyRoleException() {
        super("Empty role encountered in list!");
    }

}
