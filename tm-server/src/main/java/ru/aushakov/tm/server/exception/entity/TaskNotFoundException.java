package ru.aushakov.tm.server.exception.entity;

public class TaskNotFoundException extends RuntimeException {

    public TaskNotFoundException() {
        super("Task not found!");
    }

}
