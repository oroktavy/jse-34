package ru.aushakov.tm.server.exception.empty;

public class EmptyDataListException extends RuntimeException {

    public EmptyDataListException() {
        super("Empty data list provided!");
    }

}
