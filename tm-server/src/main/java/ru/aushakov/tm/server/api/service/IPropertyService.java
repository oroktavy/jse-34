package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.enumerated.ConfigProperty;

public interface IPropertyService {

    String getProperty(ConfigProperty property);

    Integer getIntProperty(ConfigProperty property);

}
