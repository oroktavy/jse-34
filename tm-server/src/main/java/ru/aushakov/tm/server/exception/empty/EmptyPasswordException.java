package ru.aushakov.tm.server.exception.empty;

public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Password can not be empty!");
    }

}
