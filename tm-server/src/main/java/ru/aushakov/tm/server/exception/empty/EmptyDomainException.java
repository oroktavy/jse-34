package ru.aushakov.tm.server.exception.empty;

public class EmptyDomainException extends RuntimeException {

    public EmptyDomainException() {
        super("No domain provided!");
    }

}
