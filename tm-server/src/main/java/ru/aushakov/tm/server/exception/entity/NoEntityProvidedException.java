package ru.aushakov.tm.server.exception.entity;

public class NoEntityProvidedException extends RuntimeException {

    public NoEntityProvidedException() {
        super("Null record provided for operation!");
    }

}
