package ru.aushakov.tm.server.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum Role {

    USER("User"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    @NotNull
    private static final Role[] staticValues = values();

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public static Role toRole(@Nullable final String roleId) {
        for (@NotNull final Role role : staticValues) {
            if (role.name().equalsIgnoreCase(roleId)) return role;
        }
        return null;
    }

}
