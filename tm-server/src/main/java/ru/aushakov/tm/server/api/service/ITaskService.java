package ru.aushakov.tm.server.api.service;

import ru.aushakov.tm.server.model.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    Task assignTaskToProject(String taskId, String projectId, String userId);

    Task unbindTaskFromProject(String taskId, String userId);

    List<Task> findAllTasksByProjectId(String projectId, String userId);

}
