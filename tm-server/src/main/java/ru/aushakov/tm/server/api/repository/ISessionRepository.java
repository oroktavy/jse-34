package ru.aushakov.tm.server.api.repository;

import ru.aushakov.tm.server.model.Session;

public interface ISessionRepository extends IRepository<Session> {
}
