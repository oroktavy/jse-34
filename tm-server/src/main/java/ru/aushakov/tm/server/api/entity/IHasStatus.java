package ru.aushakov.tm.server.api.entity;

import ru.aushakov.tm.server.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
