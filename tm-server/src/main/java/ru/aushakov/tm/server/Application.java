package ru.aushakov.tm.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.aushakov.tm.server.bootstrap.Bootstrap;

public class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}
